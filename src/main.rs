/*
i3rs extends support for additional functionality in i3wm.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use anyhow::{format_err, Result};
use directories::ProjectDirs;
use i3ipc::{
    event::{
        inner::{WindowChange, WorkspaceChange},
        BindingEventInfo, Event,
    },
    reply::Node,
    I3Connection, I3EventListener, ALL_SUBSCRIPTIONS,
};
use log::{debug, info, trace, warn};
use regex::Regex;
use std::collections::{HashMap, VecDeque};

fn main() -> Result<()> {
    env_logger::init();

    // Set up our settings/config object.
    let mut settings = config::Config::default();

    // Set the defaults.
    {
        let mut mark_settings: HashMap<String, config::Value> = HashMap::new();
        mark_settings.insert("back_and_forth".to_string(), config::Value::from(true));
        settings.set_default("marks", mark_settings)?;
        settings.set_default("event_buffer_length", 32)?;
    }

    // Grab any config from the XDG config directory for the app.
    if let Some(proj_dirs) = ProjectDirs::from("org", "i3rs", "i3rs") {
        // Get the actual path.
        let path = proj_dirs.config_dir();

        // If the path doesn't exist, dump a default config file there for the
        // user to edit.
        if path.exists() {
            // Add the config name, but with no extension. We'll let the config
            // crate figure out what extensions are valid/available and try them.
            let path = config::File::with_name(path.join("config").to_str().unwrap());

            // Merge the config file into the settings, if it exists.
            settings.merge(path)?;
        }
    }

    // Add in settings from the environment (with a prefix of I3RS). Eg.
    // `I3RS_EVENT_BUFFER_LENGTH=10 ./target/i3rs` would set the
    // `event_buffer_length` config key.
    settings
        .merge(config::Environment::with_prefix("I3RS"))
        .unwrap();

    // Print out the settings for debugging.
    debug!("config: {:#?}", settings);
    info!("computed configuration: {:?}", &settings.cache);

    // establish a connection to i3 over a unix socket
    let mut connection = I3Connection::connect()?;

    // request and print the i3 version
    info!("{}", connection.get_version()?.human_readable);

    // Establish a connection and subscribe to all events.
    let mut listener = I3EventListener::connect()?;
    listener.subscribe(&ALL_SUBSCRIPTIONS)?;

    // Collect received events, so we can reconstruct history and figure out
    // when we're trying to jump to an already focused mark. Use a VecDeque so
    // we can easily truncate it and push events onto the front.
    let mut events: VecDeque<Event> = VecDeque::with_capacity(settings.get("event_buffer_length")?);

    // Set up a regex for detecting mark jumps.
    let re = Regex::new(r#"\[con_mark="(.*)"\] focus"#)?;

    // Set a variable so we know whether to enable back and forth functionality.
    let back_and_forth = settings.get_table("marks")?;
    let back_and_forth = back_and_forth
        .get("back_and_forth")
        .ok_or(format_err!("missing key marks.back_and_forth"))?
        .clone()
        .into_bool()?;

    // Handle incoming events. Binding and focus events seem to come in reverse
    // order. A keybinding that focuses a window will be reported after the
    // window has been focused.
    for event in listener.listen() {
        let event = event?;

        match &event {
            Event::WorkspaceEvent(e) => {
                trace!("workspace event: {:?}", e);
                if let WorkspaceChange::Focus = e.change {
                    if let Some(current) = &e.current {
                        debug!("focusing workspace: {:?} ({})", current.name, current.id);
                    }
                }
            }
            Event::WindowEvent(e) => {
                trace!("window event: {:?}", e);
                if let WindowChange::Focus = e.change {
                    debug!(
                        "focused window: {:?} ({})",
                        e.container.name, e.container.id
                    );
                }
            }
            Event::BindingEvent(e) => {
                // We know that this is a binding event now.
                //
                // We need to figure out if this was a mark jump binding.
                if back_and_forth && re.is_match(&e.binding.command) {
                    // We now know that this is a keybinding we care about.
                    debug!("matching command: {}", &e.binding.command);

                    // Figure out which mark this is.
                    let mark = re
                        .captures(&e.binding.command)
                        .unwrap()
                        .get(1)
                        .unwrap()
                        .as_str();
                    debug!("attempting to jump to mark: {}", &mark);

                    // Get the current window tree.
                    let tree = connection.get_tree()?;
                    trace!("current tree: {:#?}", &tree);

                    // Figure out which node is focused.
                    // TODO: We could have multiple focused nodes; this will
                    // work for now on a single display.
                    let focused_node = crawl_tree(tree)?;
                    debug!("focused node in tree: {:?}", &focused_node);

                    process_binding_event(e, mark, &focused_node, &events, &mut connection)?;
                }
            }
            _ => {}
        }

        // Push the event (now that we've processed it) into our event buffer.
        events.truncate(settings.get::<usize>("event_buffer_length")? - 1);
        events.push_front(event);
        debug!(
            "events: {:#?}",
            events
                .iter()
                .enumerate()
                .map(|(i, e)| format!("{}: {}", i, &e))
                .collect::<Vec<String>>()
        );
    }

    Ok(())
}

fn crawl_tree(node: Node) -> Result<Node> {
    if node.focused {
        return Ok(node);
    }

    for node in node.nodes {
        if let Ok(node) = crawl_tree(node) {
            return Ok(node);
        }
    }

    Err(format_err!("unable to locate focused node"))
}

fn process_binding_event(
    current_event: &BindingEventInfo,
    mark: &str,
    focused_node: &Node,
    events: &VecDeque<Event>,
    connection: &mut I3Connection,
) -> Result<()> {
    // If we don't have at least 3 events, it's impossible to figure out where we
    // came from.
    if events.len() < 3 {
        debug!("not enough events in event log to figure out where to jump back to; skipping");
        return Ok(());
    }

    // Get all the configured marks.
    let marks = connection.get_marks()?;
    info!("{:?}", &marks);

    // If the currently focused node doesn't even have the
    // mark we're trying to go to, then we can ignore the event.
    // TODO: Is this possible? Maybe a jump with a mark that
    // doesn't exist?
    if !focused_node.marks.contains(&mark.to_string()) {
        debug!(
            "currently focused window does not contain mark {}; skipping",
            &mark
        );
        return Ok(());
    }

    // Get the last event we saw, prior to this one. If there's
    // no event history, just move along.
    let last_event = if let Some(Event::BindingEvent(event)) = events.front() {
        event
    } else {
        debug!("last event was not a binding event; skipping");
        return Ok(());
    };

    // If the last event we saw isn't a duplicate of this event,
    // it can't be the user mashing the same key trying to jump back.
    if last_event.binding.command != current_event.binding.command {
        debug!("last event command does not match current event command; skipping");
        return Ok(());
    }

    // If the last event we saw matches the event we're
    // processing now, it means the user tried to jump to the
    // same mark twice in a row, which we'll assume means
    // they're on the window with the mark, and trying to jump
    // back to where they came from.
    //
    // Now we need to figure out where we were focused
    // before the current node.
    let recent = if let Some(event) = events.iter().find(|e| {
        // If the event was a window change event...
        if let Event::WindowEvent(e) = e {
            match e.change {
                // ...and it was a focus event...
                WindowChange::Focus => {
                    // ... and it was a focus event on a node other than
                    // the currently focused node...
                    if e.container.id != focused_node.id {
                        // ...we assume this is where the
                        // user came from.
                        return true;
                    }
                }
                _ => return false,
            }
        }

        false
    }) {
        event
    } else {
        warn!("unable to find previously focused window");
        return Ok(());
    };

    // Now look up the actual event from the index returned by rposition.
    // TODO: Store events in reverse order so we can use normal iteration to locate nodes.
    let recent = match recent {
        Event::WindowEvent(event) => event,
        _ => panic!("expected window event"),
    };
    info!(
        "detected attempt to focus currently focused mark; jumping to {:?} instead",
        &recent.container.name,
    );

    // Go ahead and issue the command to return from whence
    // we came!
    let reply = connection.run_command(&format!("[con_id={}] focus", recent.container.id))?;
    debug!("reply to focus command: {:?}", reply);

    Ok(())
}
